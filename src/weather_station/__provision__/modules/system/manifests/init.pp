class system {
	file { '/etc/motd':
		content => 'Weather Station Platform Development Machine'
	}

	exec { 'apt-get update':
		command => 'apt-get update',
		path => '/usr/bin'
	}

	package { 'vim':
		ensure => installed,
		require => Exec['apt-get update']
	}

	package { 'python3':
		ensure => installed,
		require => Exec['apt-get update']
	}

	package { 'python3-dev':
		ensure => installed,
		require => Package['python3']
	}

	package { 'python3-pip':
		ensure => installed,
		require => Package['python3']
	}

	package { 'git':
		ensure => installed,
		require => Exec['apt-get update']
	}
}
