class supervisor {
	require system
	require project

	package { 'supervisor':
		ensure => installed
	}

	service { 'supervisor':
		enable => true,
		ensure => running,
		require => Package['supervisor'],
		# supervisor errors code bug
		hasstatus => false,
		pattern => 'supervisord'
	}

	file { '/etc/supervisor/conf.d/weather_station.conf':
		ensure => present,
		require => Package['supervisor'],
		source => 'puppet:///modules/supervisor/weather_station.conf',
		notify => Service['supervisor']
	}

	file { '/etc/default/supervisor':
		ensure => present,
		require => Package['supervisor'],
		source => 'puppet:///modules/supervisor/supervisor',
		notify => Service['supervisor']
	}
}
