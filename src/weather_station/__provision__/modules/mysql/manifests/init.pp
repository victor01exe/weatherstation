class mysql {
	require system

	package { 'mysql-server':
		ensure => installed
	}

	package { 'mysql-client':
		ensure => installed,
		require => Package['mysql-server']
	}

	package { 'libmysqlclient-dev':
		ensure => installed,
		require => Package['mysql-client']
	}

	service { 'mysql':
		enable => true,
		ensure => running,
		require => Package['mysql-server']
	}

	file { '/etc/mysql/my.cnf':
		ensure => present,
		source => 'puppet:///modules/mysql/my.cnf',
		require => Package['mysql-server'],
		notify => Service['mysql']
	}

	exec { 'create mysql user':
		command => 'mysql -u root -e "grant all on *.* to \'weather_station\'@\'%\' identified by \'weather_station\';"',
		path => '/usr/bin',
		require => Service['mysql']
	}

	exec { 'create weather_station database':
		command => 'mysql -u root -e "create database if not exists weather_station;"',
		path => '/usr/bin',
		require => Service['mysql']
	}
}
