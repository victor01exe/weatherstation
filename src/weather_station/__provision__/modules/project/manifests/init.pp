class project {
	require system
	require mysql

	exec { 'install dependencies':
		command => 'pip3 install -r /vagrant/requirements.txt',
		path => '/usr/bin'
	}

	exec { 'apply database migrations':
		command => 'python3 /vagrant/manage.py migrate',
		path => '/usr/bin',
		user => 'vagrant',
		require => Exec['install dependencies']
	}
}
