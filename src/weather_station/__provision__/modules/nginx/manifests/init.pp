class nginx {
	require system
	require project

	package { 'nginx':
		ensure => installed
	}

	service { 'nginx':
		enable => true,
		ensure => running,
		require => Package['nginx']
	}

	file { '/etc/nginx/nginx.conf':
		ensure => present,
		require => Package['nginx'],
		source => 'puppet:///modules/nginx/nginx.conf',
		notify => Service['nginx']
	}
}
