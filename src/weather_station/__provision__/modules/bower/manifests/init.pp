class bower {
	require system

	exec { 'add repo':
		command => 'curl -sL https://deb.nodesource.com/setup_0.10 | sudo -E bash -',
		path => '/usr/bin:/bin'
	}

	package { 'nodejs':
		ensure => installed,
		require => Exec['add repo']
	}

	exec { 'install bower':
		command => 'npm install -g bower',
		path => '/usr/bin',
		require => Package['nodejs']
	}
}
